import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ValidacionesService } from 'src/app/shared/validator/validaciones.service';
import {
  emailPattern,
  nombreApellidoPattern,
  noPuedeSerX,
} from 'src/app/shared/validators/validaciones';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
})
export class RegistroComponent implements OnInit {
  //?validaciones
  miFormulario: FormGroup = this.fb.group(
    {
      nombre: [
        ,
        [
          Validators.required,
          Validators.pattern(this.vls.nombreApellidoPattern),
        ],
      ],
      email: [
        '',
        [Validators.required, Validators.pattern(this.vls.emailPattern)],
      ],
      username: ['', [Validators.required, this.vls.noPuedeSerX]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      password2: ['', [Validators.required]],
    },
    {
      validators: [this.vls.camposIguales('password', 'password2')],
    }
  );

  get emailErrors(): string {
    const errors = this.miFormulario.get('email')?.errors;
    if (errors?.required) {
      return 'Email Obligatorio';
    } else if (errors?.pattern) {
      return 'Email invalido';
    }

    return 'hola';
  }

  constructor(private fb: FormBuilder, private vls: ValidacionesService) {}

  ngOnInit(): void {}
  campoNoValido(campo: string) {
    return (
      this.miFormulario.get(campo)?.invalid &&
      this.miFormulario.get(campo)?.touched
    );
  }

  submitFormulario() {
    console.log(this.miFormulario.value);
    this.miFormulario.markAllAsTouched();
  }

  emailRequired() {
    return (
      this.miFormulario.get('email')?.errors?.required &&
      this.miFormulario.get('email')?.touched
    );
  }
  emailFormato() {
    return (
      this.miFormulario.get('email')?.errors?.pattern &&
      this.miFormulario.get('email')?.touched
    );
  }
}
